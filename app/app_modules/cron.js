// Generic modules.
var portscanner = require('portscanner')
// Night Owl Modules.
const es = require("./elasticsearch.js")
const ansible = require("./ansible.js");

// Global variables, will be moved into settings later.
var PING_INTERVAL = 30;
var SCAN_INTERVAL = 30;
var AUTOSCAN = true;
var AUTOPING = true;

function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

// Interval between ping batch scans.
es.settings_get("PING_INTERVAL", (res) => {
    if (res) {
        PING_INTERVAL = res;
    } else {
        es.settings_put("PING_INTERVAL", PING_INTERVAL);
    }
});

// true/fase to automaticall ping devices.
es.settings_get("AUTOPING", (res) => {
    if (res) {
        AUTOPING = res;
    } else {
        es.settings_put("AUTOPING", AUTOPING);
    }
});

// Interval between scrape batches.
es.settings_get("SCAN_INTERVAL", (res) => {
    if (res) {
        SCAN_INTERVAL = res;
    } else {
        es.settings_put("SCAN_INTERVAL", SCAN_INTERVAL);
    }
});

// true/fase to automaticall scan device config & details.
es.settings_get("AUTOSCAN", (res) => {
    if (res) {
        AUTOSCAN = res;
    } else {
        es.settings_put("AUTOSCAN", AUTOSCAN);
    }
});

// Ping specific device.
const runPing = (devices, increment = 0) => {
    if (increment < devices.length) {
        portscanner.checkPortStatus(22, devices[increment].ip, (error, status) => {
            es.device_get(devices[increment].name, (device_refresh) => {
                device_refresh.details["reachable"] = status;
                es.device_put(
                    device_name = device_refresh.name,
                    ip = device_refresh.ip,
                    make = device_refresh.make,
                    details = device_refresh.details,
                    type = device_refresh.type,
                    credential = device_refresh.credential,
                    group = device_refresh.group,
                    callback = () => {
                        increment++;
                        runPing(devices, increment);
                    }
                )
            });
        });
    } else {
        console.log("[c] SSH Scan complete on all devices; sleeping for " + PING_INTERVAL + " minutes.");
        return;
    }
}

// Ping entire list of devices.
const pingDevices = () => {
    es.device_get("*", function (devices) {
        devices = sortByKey(devices, "name");
        if (AUTOPING) {
            runPing(devices);
        }
        setTimeout(() => {
            pingDevices();
        }, 60 * 1000 * PING_INTERVAL);
    });
}

const runScan = (devices, increment = 0) => {
    if (increment < devices.length) {
        ansible.scrape(devices[increment].name, (resp) => {
            increment++;
            runScan(devices, increment);
        });
    } else {
        console.log("[c] Device Scan complete on all devices; sleeping for " + PING_INTERVAL + " minutes.");
        return;
    }
}

const scanDevices = () => {
    es.device_get("*", function (devices) {
        devices = sortByKey(devices, "name");
        if (AUTOSCAN) {
            runScan(devices);
        }
        setTimeout(() => {
            scanDevices();
        }, 60 * 1000 * SCAN_INTERVAL);
    });
}

module.exports = {
    init: () => {
        pingDevices();
        scanDevices();
    }
}
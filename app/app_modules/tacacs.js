const { exec } = require('child_process');
const es = require('./elasticsearch.js');
const fs = require('fs');
const os = require('os');

const utils = require('./utils.js')


const root_config = `
#!/usr/local/sbin/tac_plus
id = spawnd {
    listen = { address = 0.0.0.0 port = 49 }
    spawn = {
            instances min = 1
            instances max = 10
    }
    background = no
}

id = tac_plus {
    debug = MAVIS

    access log = /var/log/tac_plus/access/%Y/%m/access-%m-%d-%Y.txt
    accounting log = /var/log/tac_plus/accounting/%Y/%m/accounting-%m-%d-%Y.txt
    authentication log = /var/log/tac_plus/authentication/%Y/%m/authentication-%m-%d-%Y.txt

    mavis module = external {
        script out = {
            if (undef($TACMEMBER) && $RESULT == ACK) set $RESULT = NAK
            if ($RESULT == ACK) set $PASSWORD_ONESHOT = 1
        }
        setenv LDAP_SERVER_TYPE = "`+ utils.env('LDAP_SERVER_TYPE', 'microsoft') + `"
        setenv LDAP_HOSTS = "`+ utils.env('LDAP_HOSTS', 'ldap://localhost') + `"
        setenv LDAP_BASE = "`+ utils.env('LDAP_BASE', 'DC=my,DC=domain') + `"
        setenv LDAP_SCOPE = "`+ utils.env('LDAP_SCOPE', 'sub') + `"
        setenv LDAP_FILTER = "`+ utils.env('LDAP_FILTER', '(&(objectClass=user)(objectClass=person)(sAMAccountName=%s))') + `"
        setenv LDAP_USER = "`+ utils.env('LDAP_USER', 'admin') + `"
        setenv LDAP_PASSWD = "`+ utils.env('LDAP_PASSWD', 'admin') + `"
        setenv AD_GROUP_PREFIX = "`+ utils.env('AD_GROUP_PREFIX', '') + `"
        setenv UNLIMIT_AD_GROUP_MEMBERSHIP = "1"
        setenv EXPAND_AD_GROUP_MEMBERSHIP = 0
        setenv REQUIRE_TACACS_GROUP_PREFIX = 0
        setenv FLAG_USE_MEMBEROF = 1
        exec = /usr/local/lib/mavis/mavis_tacplus_ldap.pl
    }

    login backend = mavis
    user backend = mavis
    connection timeout = 600
    context timeout = 3600
    password max-attempts = 5
    password backoff = 1
    separation tag = "*"
    skip conflicting groups = yes
    skip missing groups = yes
    user backend = mavis
    login backend = mavis chpass
    pap backend = mavis

    # Global host group.
    host = world {
        address = 0.0.0.0/0
        prompt = "`+ utils.env('LOGIN_PROMPT', 'Welcome! Please login to continue.') + `"
        key = "`+ utils.env('TAC_KEY', 'supersafe') + `"
    }
    {{host_list}}
}
`;

const parse_array = (array) => {
    if (array.includes(",")) {
        array = array.split(",")
    } else {
        array = [array]
    }
    return array;
}

var created_hosts = []

const write_config = (callback) => {
    console.log("[i] TACACS: Building new configuration.");
    es.settings_get("tacacs_groups", groups => {
        es.settings_get("tacacs_super_groups", super_groups => {
            es.settings_get("tacacs_read_all_groups", read_all_groups => {
                super_groups = parse_array(super_groups);
                groups = parse_array(groups);
                if (read_all_groups) {
                    read_all_groups = parse_array(read_all_groups);
                }
                es.device_get("*", (host_list) => {
                    var dynamic_config = `
                    # Network Host Definitions.`;
                    host_list.forEach(host => {
                        if (!dynamic_config.includes(host.ip.trim())) {
                            created_hosts.push(host.name.trim());
                            dynamic_config += `
                    host = `+ host.name.trim() + ` {
                        address = "`+ host.ip.trim() + `"
                        template = world
                    }
                            `;
                        }
                    });

                    var group_config = `
                    # Group Access Permissions.`;

                    if (read_all_groups) {
                        read_all_groups.forEach(group => {
                            group_config += `
                    group = `+ group.trim() + ` {
                        message = "Welcome! Your access mode is set to 'Read-Only'."`;
                            group_config += `
                        server = permit 0.0.0.0/0`;

                            group_config += `
                        default service = permit
                        # IOS
                        service = shell {
                            set priv-lvl = 5
                            default attribute = permit
                            default cmd = permit
                        }
                        # WLC
                        service = ciscowlc {
                            set role1 = ALL
                        }
                        # Fortigate
                        service = fortigate {
                            optional admin_prof = read_only
                        }
                    }`;
                        });
                    }

                    super_groups.forEach(group => {
                        group_config += `
                    group = `+ group.trim() + ` {
                        message = "Welcome SuperUser! Your access mode is set to 'Read-Write'."`;
                        host_list.forEach(host => {
                            if (true) {
                                var is_present = false;
                                created_hosts.forEach(host_check => {
                                    if (host.name.trim() == host_check) {
                                        is_present = true;
                                    }
                                });
                                if (is_present) {
                                    group_config += `
                        server = permit `+ host.name.trim() + ``;
                                }
                            }
                        });
                        group_config += `
                        server = deny 0.0.0.0/0`;

                        group_config += `
                        default service = permit
                        # IOS
                        enable = login
                        service = shell {
                            set priv-lvl = 15
                            default attribute = permit
                            default cmd = permit
                        }
                        # WLC
                        service = ciscowlc {
                            set role1 = ALL
                        }
                        # Fortigate
                        service = fortigate {
                            optional admin_prof = super_admin
                        }
                    }`;
                    });

                    groups.forEach(group => {
                        group_config += `
                    group = `+ group.trim() + ` {
                        message = "Welcome! Your access mode is set to 'Read-Write'."`;
                        host_list.forEach(host => {
                            if (host.group.trim() === group.trim()) {
                                var is_present = false;
                                created_hosts.forEach(host_check => {
                                    if (host.name.trim() == host_check) {
                                        is_present = true;
                                    }
                                });
                                if (is_present) {
                                    group_config += `
                        server = permit `+ host.name.trim() + ``;
                                }
                            }
                        });
                        group_config += `
                        server = deny 0.0.0.0/0`;

                        group_config += `
                        default service = permit
                        # IOS
                        enable = login
                        service = shell {
                            set priv-lvl = 15
                            default attribute = permit
                            default cmd = permit
                        }
                        # WLC
                        service = ciscowlc {
                            set role1 = ALL
                        }
                        # Fortigate
                        service = fortigate {
                            optional admin_prof = super_admin
                        }
                    }
                    `;
                    });

                    dynamic_config += group_config;
                    var out_config = root_config.replace("{{host_list}}", dynamic_config);
                    fs.writeFile("/usr/local/etc/tac_plus_new.cfg", out_config, (err) => {
                        callback(true);
                    });
                });
            });
        });
    });
}

var attempts = 0;
const build_config = (callback = null) => {
    //amqp.broadcast("configure");
    if (attempts < 3) {
        attempts++;
        try {
            write_config(success => {
                if (success) {
                    verify_config(config_valid => {
                        if (config_valid) {
                            apply_config(config_applied => {
                                if (config_applied) {
                                    status(is_running => {
                                        if (is_running) {
                                            restart(restart_success => {
                                                if (restart_success) {
                                                    restart(restart_success => {
                                                        if (callback) {
                                                            attempts = 0;
                                                            callback(true);
                                                        }
                                                    });
                                                } else {
                                                    build_config(callback);
                                                }
                                            });
                                        } else {
                                            start(result => {
                                                if (result) {
                                                    if (callback) {
                                                        attempts = 0;
                                                        callback(true);
                                                    }
                                                } else {
                                                    build_config(callback);
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    console.log("[e] Failed to apply config, attempt: " + attempts);
                                    build_config(callback);
                                }
                            });
                        } else {
                            console.log("[e] Failed to verify config, attempt: " + attempts);
                            build_config(callback);
                        }
                    });
                } else {
                    console.log("[e] Failed to build config, attempt: " + attempts);
                    build_config(callback);
                }
            });
        }
        catch (err) {
            console.log(`Error building config. ${err}`);
            build_config(callback);
        }
    }
}

const process_message_action = (message) => {
    switch(message) {
        case "configure":
            build_config((res)=>{
                console.log(`[i] Task ${message} complete [${res}].`);
            });
            break;
        case "reload":
            reload((res)=>{
                console.log(`[i] Task ${message} complete [${res}].`);
            });
            break;
        case "restart":
            restart((res)=>{
                console.log(`[i] Task ${message} complete [${res}].`);
            });
            break;
        case "start":
            start((res)=>{
                console.log(`[i] Task ${message} complete [${res}].`);
            });
            break;
        case "stop":
            stop((res)=>{
                console.log(`[i] Task ${message} complete [${res}].`);
            });
            break;
        default:
            console.log(`[q] Skipping request ${message}, command not recognised.`)
    }
}

var configured_devices = [];
var configured_ips = [];
const device_monitor = (callback = null) => {
    if (configured_devices.length == 0 || configured_ips.length == 0) {
        console.log("[i] Initializing device monitor...");
    }
    es.device_get("*", (devices) => {
        if (devices.length > 0) {
            var current_devices = [];
            var current_ips = [];
            var change_detected = false;
            for (var i = 0; i < devices.length; i++) {
                current_devices.push(devices[i].name);
                current_ips.push(devices[i].ip);
                if (!configured_ips.includes(devices[i].ip)) {
                    change_detected = true;
                }
                if (!configured_devices.includes(devices[i].name)) {
                    change_detected = true;
                }
            }
            if (change_detected || configured_devices.length == 0 || configured_ips.length == 0) {
                console.log("[i] Change detected in device list, building new configuration.");
                build_config(callback);
            }
            configured_devices = current_devices;
            configured_ips = current_ips;
        }
        setTimeout(() => {
            device_monitor();
        }, 5000);
    });
}

const verify_config = (callback = null) => {
    exec(`/usr/local/sbin/tac_plus -P /usr/local/etc/tac_plus_new.cfg`, (err, stdout, stderr) => {
        if (err) {
            if (callback) {
                callback(false);
            }
        } else {
            if (callback) {
                callback(true);
            }
        }
    });
}

const apply_config = (callback = null) => {
    fs.rename("/usr/local/etc/tac_plus_new.cfg", "/usr/local/etc/tac_plus.cfg", err => {
        if (err) {
            if (callback) {
                callback(false);
            }
        } else {
            console.log("[i] TACACS: New configuration applied.");
            if (callback) {
                callback(true);
            }
        }
    })
}

const status = (callback = null) => {
    exec('service tac_plus status', { timeout: 10000 }, (err, stdout, stderr) => {
        if (err || stderr) {
            if (callback) {
                callback(false);
            }
        } else {
            if (callback) {
                callback(true);
            }
        }
    });
}

const restart = (callback = null) => {
    //amqp.broadcast("restart");
    stop(stop_result => {
        start(start_result => {
            if (callback) {
                callback(start_result);
            }
        });
    });
}

const reload = (callback = null) => {
    //amqp.broadcast("reload");
    exec('service tac_plus reload', options = { timeout: 15000 }, (err, stdout, stderr) => {
        if (callback) {
            if (err) {
                callback(false);
            } else {
                callback(true);
            }
        }
    });
}

const start = (callback = null) => {
    //amqp.broadcast("start");
    exec('service tac_plus start', options = { timeout: 10000 }, (err, stdout, stderr) => {
        setTimeout(() => {
            status(
                result => {
                    if (callback) {
                        callback(result);
                    }
                }
            )
        }, 1000);
    });
}

const stop = (callback = null) => {
    //amqp.broadcast("stop");
    exec('service tac_plus stop', options = { timeout: 10000 }, (err, stdout, stderr) => {
        setTimeout(() => {
            status(
                result => {
                    if (callback) {
                        callback(!result);
                    }
                }
            )
        }, 1000);
    });
}


var startup = true;
module.exports = {
    initialize_monitor: (callback = null) => {
        //amqp.connect("NIGHT_OWL_TACACS_TASK", process_message_action);
        device_monitor(callback);
    },
    cat: (callback) => {
        fs.readFile("/usr/local/etc/tac_plus.cfg", (err, data) => {
            if (callback) {
                callback(data);
            }
        });
    },
    access: (callback = null) => {
        exec(`find /var/log/tac_plus/access | grep .txt | xargs cat`, (err, stdout, stderr) => {
            if (callback) {
                callback(stdout);
            }
        });
    },
    accounting: (callback = null) => {
        exec(`find /var/log/tac_plus/accounting | grep .txt | xargs cat`, (err, stdout, stderr) => {
            if (callback) {
                callback(stdout);
            }
        });
    },
    authentication: (callback = null) => {
        exec(`find /var/log/tac_plus/authentication | grep .txt | xargs cat`, (err, stdout, stderr) => {
            if (callback) {
                callback(stdout);
            }
        });
    },
    configure: (callback = null) => {
        build_config(callback);
    },
    status: (callback = null) => {
        status(callback);
    },
    restart: (callback = null) => {
        restart(callback);
    },
    start: (callback = null) => {
        start(callback);
    },
    stop: (callback = null) => {
        stop(callback);
    },
};
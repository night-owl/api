const cluster = require("amqp-1.0-client");
const utils = require("./utils");

const env = utils.env("NIGHT_OWL_ENV", "PROD");

module.exports = {
    clients: cluster.activeClients,
    clientIDs: cluster.activeClientIDs,
    connectAll: () => cluster.connect(`NIGHT_OWL_CLUSTER_${env}`),
    leader: cluster.leader,
    on: (task, callback) => {
        cluster.on(task, callback);
    }
}
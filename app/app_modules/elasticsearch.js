// Fetch base env variables for elasticsearch configuration.
const utils = require('./utils.js')
const uuidv4 = require('uuid/v4');

var host = utils.env("ES_HOST", "http://localhost");
var host_port = utils.env("ES_PORT", "9200");
var require_auth = utils.env("ES_AUTH_REQUIRED", false);

var unescape = require('unescape-js');

const Cryptr = require('cryptr');
const cryptr = new Cryptr(utils.env("SECRET_KEY", "svxp8CaSpWGVD5yRWftmdwwazyzXGvsu7fRb3txQJFn6dU6SDGZwmnJTTvBsByNT"));

var ready = false;

// Parse the ES_AUTH_REQUIRED paramenter to check if yes/true were actually strings.
if (require_auth === "yes" || require_auth === "true" || require_auth === true) {
    require_auth = true;
} else {
    require_auth = false;
}

var protocol = "http";
if (host.includes("://")) {
    protocol = host.split("://")[0];
    host = host.split("://")[1];
}
host = host.replace(/\/$/, "");

// Fetch auth paramenters if ES_AUTH_REQUIRED is set to true.
var auth = "";
if (require_auth) {
    auth = utils.env("ES_AUTH_USERNAME", "elastic") + ":" + utils.env("ES_AUTH_PASSWORD", "elastic") + "@";
}
auth = protocol + "://" + auth;

// Connnect to the elasticsearch node.
const { Client } = require('@elastic/elasticsearch');
const client = new Client({ node: encodeURI(auth + host + ":" + host_port) });

const indeces = [
    "night-owl-settings",
    "night-owl-log",
    "night-owl-devices",
    "night-owl-configs",
    "night-owl-credentials",
    "night-owl-users"
]

const base = () => {
    indeces.forEach(index => {
        client.indices.exists({
            index: index,
        },
            (err, resp) => {
                if (!resp.body) {
                    client.indices.create({ index: index });
                }
            })
    });
}

const parse_array = (array) => {
    if (array) {
        if (array.includes(",")) {
            array = array.split(",")
        } else {
            array = [array]
        }
        console.log(`Parsed Array: ${array}`);
        return array;
    } else {
        console.log(`Parsed Array: ${[]}`);
        return [];
    }
}
const stringify_array = (array) => {
    var datastring = null;
    array.forEach(element => {
        if (datastring) {
            datastring = `${datastring},${element}`;
        } else {
            datastring = element;
        }
    });
    return datastring;
}

const group_mapper = (group_name) => {
    switch (group_name) {
        case "user":
            return "tacacs_groups";
        case "superuser":
            return "tacacs_super_groups";
        case "readonly":
            return "tacacs_read_all_groups";
        default:
            return "tacacs_groups";
    }
}
const get_setting = (var_name, callback = null) => {
    client.search({
        body: {
            "query": {
                "terms": {
                    "_id": [var_name]
                }
            }
        }
    },
        (err, resp) => {
            try {
                if (!resp.body.hits) {
                    callback(null);
                } else {
                    callback(cryptr.decrypt(resp.body.hits.hits[0]._source.doc.value))
                }
            } catch (err) {
                callback(null)
            }
        }
    );
}

const put_setting = (var_name, value, callback = null) => {
    var enc_var = cryptr.encrypt(value);
    client.index({
        index: "night-owl-settings",
        refresh: true,
        id: var_name,
        type: "_doc",
        body: {
            doc: {
                value: enc_var
            }
        }
    }, (err, resp) => {
        if (callback) {
            callback(err, resp);
        }
    }
    );
}

module.exports = {
    // Night Owl Settings and logging.
    initialize: () => {
        base();
    },
    groups_put: (group_name, category = null, callback = null) => {
        get_setting(group_mapper(category), (groups) => {
            console.log(`Adding '${group_name}' to '${group_mapper(category)}'.`);
            var group_arr = parse_array(groups);
            group_arr.push(group_name);
            console.log({
                group_name,
                category,
                group_arr
            });
            put_setting(group_mapper(category), stringify_array(group_arr), () => {
                if (callback) {
                    callback(true);
                }
            })
        })
    },
    groups_remove: (group_name, category = null, callback = null) => {
        console.log(`Removing '${group_name}' from '${group_mapper(category)}'.`);
        get_setting(group_mapper(category), (groups) => {
            if (groups.includes(group_name)) {
                var group_arr = parse_array(groups);
                group_arr.push(group_name);
                put_setting(group_mapper(category), stringify_array(group_arr), () => {
                    if (callback) {
                        callback(true);
                    }
                })
            } else {
                if (callback) {
                    callback(true);
                }
            }
        });
    },
    settings_get: (var_name, callback) => {
        client.search({
            body: {
                "query": {
                    "terms": {
                        "_id": [var_name]
                    }
                }
            }
        },
            (err, resp) => {
                try {
                    if (!resp.body.hits) {
                        callback(null);
                    } else {
                        callback(cryptr.decrypt(resp.body.hits.hits[0]._source.doc.value))
                    }
                } catch (err) {
                    callback(null)
                }
            }
        );
    },
    settings_put: (var_name, value, callback = null) => {
        var enc_var = cryptr.encrypt(value);
        client.index({
            index: "night-owl-settings",
            refresh: true,
            id: var_name,
            type: "_doc",
            body: {
                doc: {
                    value: enc_var
                }
            }
        }, (err, resp) => {
            if (callback) {
                callback(err, resp);
            }
        }
        );
    },
    log_put: (source, log, callback) => {

    },
    // Device related information and logs.
    device_get: (device_name, callback) => {
        if (device_name === "*") {
            client.search({
                index: "night-owl-devices",
                size: 10000,
                body: {
                    "query": {
                        match_all: {}
                    }
                }
            },
                (err, resp) => {
                    try {
                        items = []
                        if (!resp.body.hits) {
                            callback([]);
                        } else {
                            for (var i = 0; i < resp.body.hits.hits.length; i++) {
                                resp.body.hits.hits[i]._source.doc.name = resp.body.hits.hits[i]._id
                                if (resp.body.hits.hits[i]._source.doc) {
                                    items.push(resp.body.hits.hits[i]._source.doc)
                                }
                            }
                        }
                        callback(items);
                    } catch (err) {
                        console.log("[e] Error fetching device list: " + err);
                        callback([]);
                    }
                }
            );
        } else {
            client.search({
                index: "night-owl-devices",
                body: {
                    "query": {
                        "terms": {
                            "_id": [device_name]
                        }
                    }
                }
            },
                (err, resp) => {
                    try {
                        if (!resp.body.hits) {
                            callback([]);
                        } else {
                            resp.body.hits.hits[0]._source.doc.name = resp.body.hits.hits[0]._id;
                            callback(resp.body.hits.hits[0]._source.doc)
                        }
                    } catch (err) {
                        callback([])
                    }
                }
            );
        }

    },
    device_put: (device_name, ip, make, details, type, credential = "default", group = "default", callback = null) => {
        client.index({
            index: "night-owl-devices",
            refresh: true,
            id: device_name,
            type: "_doc",
            body: {
                doc: {
                    ip: ip,
                    make: make,
                    details: details,
                    type: type,
                    credential: credential,
                    group: group
                }
            }
        }, (err, resp) => {
            err ? console.log(`ERROR: ${err} ${JSON.stringify(resp.body)}`) : console.log(`RESP: ${resp}`);
            if (callback) {
                callback(err, resp);
            }
        }
        );
    },
    device_delete: (device_name) => {
        try {
            client.deleteByQuery({
                index: "night-owl-devices",
                body: {
                    "query": {
                        "terms": {
                            "_id": [device_name]
                        }
                    }
                }
            },
                () => {
                    return true;
                }
            )
        } catch (err) {
            console.log("[e] " + err);
            return false;
        }

    },
    config_get: (device_name, callback) => {
        client.search({
            index: "night-owl-configs",
            body: {
                "query": {
                    "terms": {
                        "doc.device.keyword": [device_name]
                    }
                }
            }
        },
            (err, resp) => {
                try {
                    if (!resp.body.hits) {
                        callback([]);
                    } else {
                        var hits = resp.body.hits.hits;
                        var configs = [];
                        for (var i = 0; i < hits.length; i++) {
                            var hit_object = hits[i]._source
                            configs.push(
                                {
                                    "timestamp": hit_object.timestamp,
                                    "config": unescape(hit_object.doc.config)
                                }
                            )
                        }
                        callback(configs);
                    }
                } catch (err) {
                    console.log("[e] Error parsing config list: " + err);
                    callback([]);
                }
            }
        );
    },
    config_put: (device_name, config, callback) => {
        client.index({
            index: "night-owl-configs",
            refresh: true,
            id: uuidv4(),
            type: "_doc",
            body: {
                timestamp: Date.now(),
                doc: {
                    device: device_name,
                    config: config.replace(/\r\n|\n\r|\n|\r/g, '\n')
                }
            }
        }, (err, resp) => {
            if (callback) {
                callback(err, resp);
            }
        }
        );
    },
    config_delete: (config_id) => {
        try {
            client.deleteByQuery({
                index: "night-owl-configs",
                body: {
                    "query": {
                        "terms": {
                            "_id": [config_id]
                        }
                    }
                }
            },
                () => {
                    return true;
                }
            )
        } catch (err) {
            console.log("[e] " + err);
            return false;
        }
    },
    credentials_get: (cred_name, callback) => {
        if (cred_name === "*") {
            client.search({
                index: "night-owl-credentials",
                body: {
                    "query": {
                        match_all: {}
                    }
                }
            },
                (err, resp) => {
                    try {
                        if (!resp.body.hits) {
                            callback([]);
                        } else {
                            items = []
                            for (var i = 0; i < resp.body.hits.hits.length; i++) {
                                items.push(resp.body.hits.hits[i]._id);
                            }
                            callback(items);
                        }
                    } catch (err) {
                        callback([])
                    }
                }
            );
        } else {
            client.search({
                index: "night-owl-credentials",
                body: {
                    "query": {
                        "terms": {
                            "_id": [cred_name]
                        }
                    }
                }
            },
                (err, resp) => {
                    try {
                        try {
                            if (!resp.body.hits) {
                                callback({});
                            } else {
                                callback({
                                    "username": cryptr.decrypt(resp.body.hits.hits[0]._source.doc.username),
                                    "password": cryptr.decrypt(resp.body.hits.hits[0]._source.doc.password)
                                })
                            }
                        } catch (err) {
                            callback({})
                        }
                    } catch (err) {
                        callback({})
                    }
                }
            );
        }
    },
    credentials_delete: (cred_name) => {
        try {
            client.deleteByQuery({
                index: "night-owl-credentials",
                body: {
                    "query": {
                        "terms": {
                            "_id": [cred_name]
                        }
                    }
                }
            },
                () => {
                    return true;
                }
            )
        } catch (err) {
            console.log("[e] " + err);
            return false;
        }
    },
    credentials_put: (cred_name, username, password, callback = null) => {
        username = cryptr.encrypt(username);
        password = cryptr.encrypt(password);
        client.index({
            index: "night-owl-credentials",
            refresh: true,
            id: cred_name,
            type: "_doc",
            body: {
                doc: {
                    username: username,
                    password: password
                }
            }
        }, (err, resp) => {
            if (callback) {
                callback(err, resp);
            }
        }
        );
    },
    log_get: (range, total, callback) => {
        client.search({
            index: "night-owl-logs",
            body: {
                "query": {
                    "range": {
                        "@timestamp": {
                            "gte": `now-${range}m`
                        }
                    }
                },
                "size": total,
                "sort": [
                    {
                        "@timestamp": {
                            "order": "desc"
                        }
                    }
                ]
            }
        },
            (err, resp) => {
                try {
                    try {
                        if(!resp.body.hits) {
                            callback([]);
                        }else {
                            callback(resp.body.hits);
                        }
                    } catch (err) {
                        callback([])
                    }
                } catch (err) {
                    callback([])
                }
            }
        );
    },
    log_put: (source, category, src, dst, log_data, callback = null) => {
        client.index({
            index: "night-owl-logs",
            refresh: true,
            id: uuidv4(),
            type: "_doc",
            body: {
                "@timestamp": new Date(),
                doc: {
                    instance: source,
                    log: log_data,
                    src: src,
                    dst: dst,
                    category: category
                }
            }
        }, (err, resp) => {
            if (callback) {
                callback(err, resp);
            }
        }
        );
    },
}
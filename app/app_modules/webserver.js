const express = require('express');
var bodyParser = require("body-parser");
var cors = require('cors');
const app = express();
const es = require("./elasticsearch.js");
const tacacs = require("./tacacs.js");
const ansible = require("./ansible.js");
const utils = require("./utils.js");
const diff = require('git-diff');

const cluster = require("./cluster");
cluster.connectAll();

const basic_auth = require('express-basic-auth');

const admin_username = utils.env("API_ADMIN_USER", "admin");
const admin_password = utils.env("API_ADMIN_PASSWORD", "admin");

app.use(cors());

app.get('/healthz', (req, res) => {
    res.status(200).send("ok");
});

app.use(basic_auth({
    users: {
        [admin_username]: admin_password
    },
    unauthorizedResponse: {
        401: "Access denied. This resource is protected."
    }
}))

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/api/modules/ansible/version", (req, res) => {
    res.send({
        "version": ansible.version()
    })
})

app.get("/api/cluster/clients", (req, res) => {
    var clients = cluster.clients || {};
    res.send({
        "clients": clients || {},
        "ids": Object.keys(clients) || []
    });
});

app.get('/api/devices', (req, res) => {
    try {
        es.device_get("*", (data) => {
            res.send(data);
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.get('/api/modules', (req, res) => {
    try {
        res.send(utils.installed_modules());
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.get('/api/devices/*/scan', (req, res) => {
    try {
        var parts = req.path.split('/');
        ansible.scrape(parts[parts.length - 2], (resp) => {
            res.send(resp);
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.get('/api/devices/*/config/diff', (req, res) => {
    try {
        var parts = req.path.split('/');
        console.log("[i] Diff search requested for " + parts[parts.length - 3])
        es.config_get(parts[parts.length - 3], (data) => {
            var diff_list = [];
            if (data && data.length > 1) {
                for (var i = 1; i < data.length; i++) {
                    var diff_result = diff(data[i].config, data[i - 1].config, { flags: '--diff-algorithm=minimal --ignore-all-space --unified=1' });
                    if (diff_result === undefined) {
                        console.log("[i] No diff, just adding config.")
                        diff_result = data[i].config;
                    }
                    lines = diff_result.split(/\r\n|\r|\n/);
                    lines_modified = 0;
                    for (var j = 0; j < lines.length; j++) {
                        lines[j] = lines[j].trim();
                        if (lines[j].startsWith("+") || lines[j].startsWith("-")) {
                            lines_modified++;
                        }
                    }
                    diff_list.push({
                        result: diff_result,
                        changes: lines_modified,
                        config_1: data[i - 1].timestamp,
                        config_2: data[i].timestamp
                    })
                }
                res.send(diff_list);
            } else {
                res.send(
                    {
                        success: false,
                        msg: "Looks like there isn't enough data to perform a diff."
                    }
                );
            }
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.get('/api/settings/*', (req, res) => {
    try {
        var parts = req.path.split('/');
        console.log("[i] Fetching setting: " + parts[parts.length - 1]);
        es.settings_get(parts[parts.length - 1], (data) => {
            res.send(data);
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.post('/api/settings/*', (req, res) => {
    try {
        var parts = req.path.split('/');
        es.settings_put(parts[parts.length - 1], req.body.value, () => {
            res.send(parts[parts.length - 1] + " SAVED.");
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.post('/api/groups/*', (req, res) => {
    try {
        var parts = req.path.split('/');
        var category = req.body.category || null;
        es.groups_put(parts[parts.length - 1], category, () => {
            res.send(parts[parts.length - 1] + " SAVED.");
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
        console.log(err);
    }
});

app.delete('/api/groups/*', (req, res) => {
    try {
        var parts = req.path.split('/');
        var category = req.body.category || null;
        es.groups_remove(parts[parts.length - 1], category, () => {
            res.send(parts[parts.length - 1] + " REMOVED.");
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
        console.log(err);
    }
});

app.get('/api/devices/*/config', (req, res) => {
    try {
        var parts = req.path.split('/');
        console.log("[i] Config search requested for " + parts[parts.length - 2])
        es.config_get(parts[parts.length - 2], (data) => {
            res.send(data);
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.delete('/api/devices/*/config', (req, res) => {
    try {
        var parts = req.path.split('/');
        es.config_delete(parts[parts.length - 2], (data) => {
            res.send(data);
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.post('/api/devices/*', (req, res) => {
    try {
        var parts = req.path.split('/');
        console.log("[i] Adding device " + parts[parts.length - 1]);
        if (req.body.group) {
            console.log("[i] Taggin device with group " + req.body.group);
        } else {
            req.body.group = "default";
        }
        es.device_put(parts[parts.length - 1], req.body.ip, req.body.make, {}, req.body.type, req.body.credential, req.body.group);
        res.send(
            {
                success: true,
                msg: "Device '" + parts[parts.length - 1] + "' saved."
            }
        )
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.delete('/api/devices/*', (req, res) => {
    try {
        var parts = req.path.split('/');
        res.send({
            success: es.device_delete(parts[parts.length - 1])
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.get('/api/devices/*', (req, res) => {
    try {
        var parts = req.path.split('/');
        es.device_get(parts[parts.length - 1], (data) => {
            res.send(data);
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }

});

app.get('/api/credentials', (req, res) => {
    try {
        es.credentials_get("*", (data) => {
            res.send(data);
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.delete('/api/credentials/*', (req, res) => {
    try {
        var parts = req.path.split('/');
        res.send({
            success: es.credentials_delete(parts[parts.length - 1])
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }

});

app.post('/api/credentials/*', (req, res) => {
    var parts = req.path.split('/');
    try {
        es.credentials_put(parts[parts.length - 1], req.body.username, req.body.password);
        res.send(
            {
                success: true,
                msg: "Credential '" + parts[parts.length - 1] + "' saved."
            }
        )
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.get('/api/credentials/*', (req, res) => {
    try {
        var parts = req.path.split('/');
        es.credentials_get(parts[parts.length - 1], (data) => {
            res.send(data);
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.get('/api/tacacs/status', (req, res) => {
    tacacs.status(status => {
        res.send(status);
    })
});

app.get('/api/tacacs/show', (req, res) => {
    tacacs.cat(data => {
        res.send(data);
    })
});

app.get('/api/tacacs/logs', (req, res) => {
    es.log_get(req.header("range") || 5, req.header("total") || 250, (logData) => {
        res.send(logData);
    })
});

app.get('/api/tacacs/start', (req, res) => {
    try {
        tacacs.start(status => {
            res.send(status);
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.get('/api/tacacs/stop', (req, res) => {
    try {
        tacacs.stop(status => {
            res.send(status);
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.get('/api/tacacs/restart', (req, res) => {
    try {
        tacacs.restart(status => {
            res.send(status);
        });
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.get('/api/tacacs/configure', (req, res) => {
    try {
        tacacs.configure(status => {
            res.send(status);
        })
    } catch (err) {
        res.status(500).send("There was an error performing your request.");
    }
});

app.post("*", (req, res) => {
    res.status(404).send("Sorry, we couldn't find the resource you are looking for.");
});

app.use(express.static(__dirname + "/static"));
const listen_port = utils.env("WEBSERVER_PORT", 5000);

module.exports = {
    // Night Owl Settings and logging.
    initialize: () => {
        try {
            app.listen(listen_port);
            console.log("[i] Listening on http://localhost:" + listen_port);
        } catch (err) {
            console.log(`[i] Port ${listen_port} busy, skipping webserver (Runnig as a backend worker).`)
        }
    },
}
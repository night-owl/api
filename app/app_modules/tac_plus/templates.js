const utils = require('../utils.js');
const removeTabs = require('remove-tabs');

const LDAP_SERVER_TYPE = utils.env('LDAP_SERVER_TYPE', 'microsoft');
const LDAP_HOSTS = utils.env('LDAP_HOSTS', 'ldap://localhost');
const LDAP_BASE = utils.env('LDAP_BASE', 'DC=my,DC=domain');
const LDAP_SCOPE = utils.env('LDAP_SCOPE', 'sub');
const LDAP_FILTER = utils.env('LDAP_FILTER', '(&(objectClass=user)(objectClass=person)(sAMAccountName=%s))');
const LDAP_USER = utils.env('LDAP_USER', 'admin');
const LDAP_PASSWD = utils.env('LDAP_PASSWD', 'admin');
const AD_GROUP_PREFIX = utils.env('AD_GROUP_PREFIX', '');
const LOGIN_PROMPT = utils.env('LOGIN_PROMPT', 'Welcome! Please login to continue.');
const TAC_KEY = utils.env('TAC_KEY', 'supersafe');

readWritePermissions = `
    default service = permit
    # Cisco, IOS.
    enable = login
    service = shell {
        set priv-lvl = 15
        default attribute = permit
        default cmd = permit
    }
    # Cisco, WLC.
    service = ciscowlc {
        set role1 = ALL
    }
    # Fortinet, Fortigate.
    service = fortigate {
        optional admin_prof = super_admin
    }
`;

readOnlyPermissions = `
    default service = permit
    # Cisco, IOS.
    service = shell {
        set priv-lvl = 5
        default attribute = permit
        default cmd = permit
    }
    # Cisco, WLC.
    service = ciscowlc {
        set role1 = ALL
    }
    # Fortinet, Fortigate.
    service = fortigate {
        optional admin_prof = read_only
    }
`;

/**
 * Get the correct template based on priviledge. 
*/
const getPrivTemplate = (writeAccess) => {
    if (writeAccess) {
        return readWritePermissions;
    }
    return readOnlyPermissions;
}
/**
 * Get the greeting based on group permissions.
*/
const getPrivGreeting = (writeAccess, groupName) => {
    var privGreeting = `Welcome ${groupName}! Priviledge Level: Read`;
    if (writeAccess) {
        privGreeting += ", Write";
    }
    return privGreeting;
}

/**
 * Get the list of host entries.
 * @param {array} hostObjects - List of all hosts.
 * 
 * hostObjects: [
 *  {
 *      "name" : "HOSTNAME",
 *      "ip" : "IP_ADDRESS"
 *  }
 * ]
*/
exports.getHostConfig = (hostObjects) => {
    var hostConfig = "";
    for(var host of hostObjects) {
        hostConfig += `
    host = ${host.name.trim()} {
        address = "${host.ip.trim()}"
        template = world
    }`
    }
    return hostConfig;
}

/**
 * Get the specific configuration for a LDAP group.
 * @param {string} groupName - Name of the LDAP group used for the configuration.
 * @param {boolean} writeAccess - Should the group have read-only or write permissions.
 * @param {array} hostObjects - List of hosts that should be part of the group.
 * 
 * hostObjects: [
 *  {
 *      "name" : "HOSTNAME",
 *      "ip" : "IP_ADDRESS"
 *  }
 * ]
*/
exports.getGroupConfig = (groupName, writeAccess = false, hostObjects) => {
    var groupConfig = `
    # Host configuration for Group: '${groupName}', Write Access: '${writeAccess}'.
    group = ${groupName} {
        message = "${getPrivGreeting(writeAccess, groupName)}"`;
    for(var host of hostObjects) {
        groupConfig += `
        server = permit ${host.name.trim()}`;
    }
    groupConfig += `
        server = deny 0.0.0.0/0`;
    groupConfig += `
        ${getPrivTemplate(writeAccess)}
    }`;
    return groupConfig;
}

/**
 * Get the full 'Tac Plus' configuration.
 * @param {string} hostConfig - Basic host configuration for the allowed clients (use getHostConfig).
 * @param {string} groupsConfig - Full group configurations (use getGroupConfigFull).
*/
exports.getConfig = (hostConfig, groupsConfig) => {
    return removeTabs`
    #!/usr/local/sbin/tac_plus
    id = spawnd {
        listen = { address = 0.0.0.0 port = 49 }
        spawn = {
                instances min = 1
                instances max = 10
        }
        background = no
    }

    id = tac_plus {
        debug = MAVIS

        access log = /var/log/tac_plus/access/%Y/%m/access-%m-%d-%Y.txt
        accounting log = /var/log/tac_plus/accounting/%Y/%m/accounting-%m-%d-%Y.txt
        authentication log = /var/log/tac_plus/authentication/%Y/%m/authentication-%m-%d-%Y.txt

        mavis module = external {
            script out = {
                if (undef($TACMEMBER) && $RESULT == ACK) set $RESULT = NAK
                if ($RESULT == ACK) set $PASSWORD_ONESHOT = 1
            }
            setenv LDAP_SERVER_TYPE = "${LDAP_SERVER_TYPE}"
            setenv LDAP_HOSTS = "${LDAP_HOSTS}"
            setenv LDAP_BASE = "${LDAP_BASE}"
            setenv LDAP_SCOPE = "${LDAP_SCOPE}"
            setenv LDAP_FILTER = "${LDAP_FILTER}"
            setenv LDAP_USER = "${LDAP_USER}"
            setenv LDAP_PASSWD = "${LDAP_PASSWD}"
            setenv AD_GROUP_PREFIX = "${AD_GROUP_PREFIX}"
            setenv UNLIMIT_AD_GROUP_MEMBERSHIP = "1"
            setenv EXPAND_AD_GROUP_MEMBERSHIP = 0
            setenv REQUIRE_TACACS_GROUP_PREFIX = 0
            setenv FLAG_USE_MEMBEROF = 1
            exec = /usr/local/lib/mavis/mavis_tacplus_ldap.pl
        }

        login backend = mavis
        user backend = mavis
        connection timeout = 600
        context timeout = 3600
        password max-attempts = 5
        password backoff = 1
        separation tag = "*"
        skip conflicting groups = yes
        skip missing groups = yes
        user backend = mavis
        login backend = mavis chpass
        pap backend = mavis

        # Global host group.
        host = world {
            address = 0.0.0.0/0
            prompt = "${LOGIN_PROMPT}"
            key = "${TAC_KEY}"
        }

        ${hostConfig}
        ${groupsConfig}
    }
    `;
}
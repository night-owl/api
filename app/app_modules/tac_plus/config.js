const fs = require('fs');
const es = require('../elasticsearch.js');
const templates = require('./templates');

/**
 * Split devices by group and return a list of groups and the parsed device lists.
 * @param {array} deviceList - Full list of all devices.
 * @return {array} List of all groups.
 * @return {Object} List of all devices split into groups.
*/
const filterDevices = (deviceList) => {
    var sorted = {};
    for (var device of deviceList){
        if (!sorted[device.group]){
            sorted[device.group] = []
        }
        sorted[device.group].push(device);
    }
    return Object.keys(sorted), sorted;
}

/**
 * Build the full Tac Plus configuration and write it to file.
*/
exports.buildConfiguration = () => {
    es.device_get("*", (deviceList) => {
        var groups, sorted = filterDevices(filterDevices);
        for (var group of groups) {
            templates.getGroupConfig(group, )
        }
    });
}
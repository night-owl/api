const templates = require("../templates");

var host_config = templates.getHostConfig([
    {
        "name": "TEST-HOST-01",
        "ip": "192.168.0.1"
    }
]);

var group_config = templates.getGroupConfig("TEST_ACS_GROUP", true, [
    {
        "name": "TEST-HOST-01",
        "ip": "192.168.0.1"
    }
]);

console.log(templates.getConfig(host_config, group_config));


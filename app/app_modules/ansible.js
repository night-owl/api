const { exec } = require('child_process');
const es = require('./elasticsearch.js')
var add_host = __dirname + '/ansible-playbooks/add_host.yml';
var inventory = __dirname + '/ansible-extras/inventory';

const substring_ansible_output = (raw) => {
    const start_delimeter = "TASK [CONFIG-PRINT] ************************************************************";
    const end_delimeter = "PLAY RECAP *********************************************************************";
    var out = raw.substring(raw.indexOf(start_delimeter) + start_delimeter.length,raw.indexOf(end_delimeter));
    var out = "{" + out.substring(out.indexOf(`"result": {`));
    return out;
}

const parse_ansible_output = (raw) => {
    try{
        return JSON.parse(raw);
    }catch (err){
        return null; 
    }
}

const add_device_to_inventory = (device, credential, callback) => {    
    exec(`ansible-playbook ${add_host} --extra-vars "inventory_file=${inventory} host_ip=${device.ip} make=${device.make}"`, (err, stdout, stderr) => {
        callback(device, credential);
    });
}

const execute_device_playbook = (playbook, device, credential, callback) => {    
    exec(`ansible-playbook ${playbook} --extra-vars "device_type=${device.type} host_ip=${device.ip} host_credential_username=${credential.username} host_credential_password=${credential.password}" -i "${inventory}"`, {maxBuffer: 10485760},(err, stdout, stderr) => {
        callback(stdout);
    });
}

const parse_fortinet_output = (raw) => {
    try {
        var new_json = {
            result: {
                ansible_facts: {
                    ansible_net_version: "",
                    ansible_net_model: "",
                    ansible_net_serialnum: "",
                    ansible_net_config: ""
                }
            }
        }
        if (raw.result.ansible_facts.ansible_net_system){
            var lines = raw.result.ansible_facts.ansible_net_system.stdout_lines;
            for (var j = 0; j < lines.length; j++) {
                if (lines[j].includes("Version:")){
                    new_json.result.ansible_facts.ansible_net_version = lines[j].substring(lines[j].indexOf("Version:") + "Version:".length, lines[j].length);
                }
                else if (lines[j].includes("System Part-Number:")){
                    new_json.result.ansible_facts.ansible_net_model = lines[j].substring(lines[j].indexOf("System Part-Number:") + "System Part-Number:".length, lines[j].length);
                }
                else if (lines[j].includes("Serial-Number:")){
                    new_json.result.ansible_facts.ansible_net_serialnum = lines[j].substring(lines[j].indexOf("Serial-Number:") + "Serial-Number:".length, lines[j].length);
                }
            }
            new_json.result.ansible_facts.ansible_net_config = raw.result.ansible_facts.ansible_net_config.stdout;
            return new_json;
        }else {
            return raw;
        }
    } catch (err) {
        //console.log("[e] " + err);
    }
    return raw;
}

const save_scrape = (device_name, device, out_json, parsed_config)  => {
    try{
        if (out_json.result.ansible_facts.ansible_net_version.Version){
            out_json.result.ansible_facts.ansible_net_version = out_json.result.ansible_facts.ansible_net_version.Version[0]['SW Version']
        }
    }catch(err) {
        //Nothing
    }    
    es.device_put(device_name, device.ip, device.make, {
        "model": out_json.result.ansible_facts.ansible_net_model,
        "serial-number": out_json.result.ansible_facts.ansible_net_serialnum,
        "os-version": out_json.result.ansible_facts.ansible_net_version,
        "hostname": device_name,
        "last_scrape": (new Date()).toUTCString()
    }, device.type, device.credential, device.group);
    es.config_put(device_name, parsed_config);
}

const scrape_device  = (device_name, device, callback) => {
    try{
        if (device.type == "dellos") {
            throw Error("Dellos device scan disabled");
        }
        var playbook = __dirname + '/ansible-playbooks/'+device.type+'.yml';
        es.credentials_get(device.credential, (credential) => {
            if(credential){
                try {
                    add_device_to_inventory(device, credential, (device, credential) => {
                        execute_device_playbook(playbook, device, credential, (resp) => {
                            raw_resp = resp;
                            resp = substring_ansible_output(resp);
                            out_json = parse_ansible_output(resp);
                            if (out_json){
                                out_json = parse_fortinet_output(out_json);
                                var parsed_config = out_json.result.ansible_facts.ansible_net_config;
                                try {
                                    parsed_config = out_json.result.ansible_facts.ansible_net_config.split("\\n").join("\n");
                                }catch (err){
                                    //
                                }
                                out_json.result.ansible_facts.ansible_net_config = parsed_config;
                                save_scrape(device_name, device, out_json, parsed_config);
                                callback(out_json.result.ansible_facts);
                            }else{
                                callback({
                                    "error":"Scan on device " + device_name + " failed. please check the configuration and try again.",
                                    "raw": raw_resp
                                });
                            }                
                        });
                    });
                }catch (err){
                    //console.log("[e] Error scraping device: " + err);
                    callback(err);
                }
            }else {
                callback("[e] Invalid credentails, skipping scan.")
            }            
        });
    }catch (err){
        //console.log("[e] Error scraping device: " + err);
        callback(err);
    }
    
}

const ansibleVersionRE = /(ansible [0-9]+.[0-9]+.[0-9]+)/gm;
var ansibleVersion = "0.0.0";

const getAnsibleVersion = () => {
    exec(`ansible --version`, (err, stdout, stderr) => {
        ansibleVersion = stdout.match(ansibleVersionRE)[0]
    });
}
getAnsibleVersion();

module.exports = {
    scrape: (device_name, callback) => {
        //console.log("[i] Scrape requested for " + device_name);
        es.device_get(device_name, (device) => {
            scrape_device(device_name, device, callback);
        });
    },
    version: () => {
        return ansibleVersion;
    }
};